package com.example.nycschool.schoolDetails;

import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import com.example.nycschool.R;
import com.example.nycschool.schoolList.SchoolData;
import com.example.nycschool.utils.Constants;

public class SchoolDetailsActivity extends AppCompatActivity {

    TextView schoolName;
    TextView dbn;
    TextView address;
    TextView city;
    TextView number;
    TextView description;
    TextView criticalReadAverage;
    TextView mathAverageScore;
    TextView writeAverageScore;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_school_detail);

        SchoolData schoolData = getIntent().getParcelableExtra(Constants.KEY_SCHOOL_DATA);
        SchoolsDetails schoolsDetails = getIntent().getParcelableExtra(Constants.KEY_SCHOOL_DETAIL);

        schoolName = findViewById(R.id.tv_school_name);
        dbn = findViewById(R.id.tv_dbn);
        address = findViewById(R.id.tv_address);
        city = findViewById(R.id.tv_city);
        number = findViewById(R.id.tv_number);
        description = findViewById(R.id.tv_description);
        criticalReadAverage = findViewById(R.id.tv_crt_red_avg_score_val);
        mathAverageScore = findViewById(R.id.tv_mat_avg_score_val);
        writeAverageScore = findViewById(R.id.tv_wrt_avg_score_val);

        loadUI(schoolData, schoolsDetails);
    }

    private void loadUI(SchoolData schoolData, SchoolsDetails schoolsDetails) {
        schoolName.setText(schoolData.getName());
        dbn.setText(schoolData.getDbn());
        address.setText(schoolData.getAddress());
         city.setText(schoolData.getCity());
        number.setText(schoolData.getNumber());
        description.setText(schoolData.getOverview());
        if(schoolsDetails != null) {
            criticalReadAverage.setText(schoolsDetails.getSatCriticalReadingAvgScore());
            mathAverageScore.setText(schoolsDetails.getSatMathAvgScore());
            writeAverageScore.setText(schoolsDetails.getSatWritingAvgScore());

            Toast.makeText(this, "School data added", Toast.LENGTH_SHORT).show();
            Toast.makeText(this,"extra toast", Toast.LENGTH_SHORT).show();
        }
    }
}
