package com.example.nycschool.schoolDetails;


import android.os.Parcel;
import android.os.Parcelable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import com.example.nycschool.utils.Constants;
import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.NotNull;

/**
 * Schools details POJO model.
 */
@Entity(tableName = "school_details")
public class SchoolsDetails implements Parcelable {

    @SerializedName(Constants.SCHOOL_DBN)
    @PrimaryKey
    @NotNull
    private String dbn;

    @SerializedName(Constants.SAT_CRITICAL_RED_AVG_SCORE)
    private String satCriticalReadingAvgScore;

    @SerializedName(Constants.SAT_MATH_AVG_SCORE)
    private String satMathAvgScore;

    @SerializedName(Constants.SAT_WRT_AVG_SCORE)
    private String satWritingAvgScore;

    @SerializedName(Constants.SCHOOL_NAME)
    private String schoolName;

    public SchoolsDetails(){

    }

    protected SchoolsDetails(Parcel in) {
        dbn = in.readString();
        satCriticalReadingAvgScore = in.readString();
        satMathAvgScore = in.readString();
        satWritingAvgScore = in.readString();
        schoolName = in.readString();
    }

    public static final Creator<SchoolsDetails> CREATOR = new Creator<SchoolsDetails>() {
        @Override
        public SchoolsDetails createFromParcel(Parcel in) {
            return new SchoolsDetails(in);
        }

        @Override
        public SchoolsDetails[] newArray(int size) {
            return new SchoolsDetails[size];
        }
    };

    public String getDbn() {
        return dbn;
    }

    public void setDbn(String dbn) {
        this.dbn = dbn;
    }

    public String getSatCriticalReadingAvgScore() {
        return satCriticalReadingAvgScore;
    }

    public void setSatCriticalReadingAvgScore(String satCriticalReadingAvgScore) {
        this.satCriticalReadingAvgScore = satCriticalReadingAvgScore;
    }

    public String getSatMathAvgScore() {
        return satMathAvgScore;
    }

    public void setSatMathAvgScore(String satMathAvgScore) {
        this.satMathAvgScore = satMathAvgScore;
    }

    public String getSatWritingAvgScore() {
        return satWritingAvgScore;
    }

    public void setSatWritingAvgScore(String satWritingAvgScore) {
        this.satWritingAvgScore = satWritingAvgScore;
    }

    public String getSchoolName() {
        return schoolName;
    }

    public void setSchoolName(String schoolName) {
        this.schoolName = schoolName;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(dbn);
        dest.writeString(satCriticalReadingAvgScore);
        dest.writeString(satMathAvgScore);
        dest.writeString(satWritingAvgScore);
        dest.writeString(schoolName);
    }
}
