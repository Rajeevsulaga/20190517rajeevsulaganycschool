package com.example.nycschool.api;

import com.example.nycschool.schoolDetails.SchoolsDetails;
import com.example.nycschool.schoolList.SchoolData;
import com.example.nycschool.utils.Constants;
import io.reactivex.Single;
import retrofit2.http.GET;

import java.util.List;

public interface SchoolDataService {

    @GET(Constants.SCHOOL_NAMES_URL)
    Single<List<SchoolData>> fetchSchoolData();

    @GET(Constants.SCHOOL_DETAILS_URL)
    Single<List<SchoolsDetails>> fetchSchoolDetails();

}
