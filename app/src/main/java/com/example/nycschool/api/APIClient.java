package com.example.nycschool.api;

import android.content.Context;
import android.text.TextUtils;
import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import kotlin.jvm.internal.Intrinsics;
import okhttp3.Interceptor;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okhttp3.logging.HttpLoggingInterceptor;
import org.jetbrains.annotations.NotNull;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

import java.io.IOException;
import java.util.concurrent.TimeUnit;

public class APIClient {

    private Retrofit retrofit;
    private final int REQUEST_TIMEOUT = 60;
    private OkHttpClient okHttpClient;

    public final Retrofit getClient(Context context, String baseUrl) {
        if (null == okHttpClient) {
            initOkHttp(context);
        }

        if (null == retrofit) {
            if (TextUtils.isEmpty(baseUrl)) {
                retrofit = new Retrofit.Builder()
                        .client(okHttpClient)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .baseUrl("https://google.com/")
                        .build();
            } else {
                retrofit = new Retrofit.Builder()
                        .client(okHttpClient)
                        .baseUrl(baseUrl)
                        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                        .addConverterFactory(GsonConverterFactory.create())
                        .build();
            }
        }

        return retrofit;
    }


    private final void initOkHttp(Context context) {
        okhttp3.OkHttpClient.Builder httpClient = (new OkHttpClient())
                .newBuilder()
                .connectTimeout((long) this.REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .readTimeout((long) this.REQUEST_TIMEOUT, TimeUnit.SECONDS)
                .writeTimeout((long) this.REQUEST_TIMEOUT, TimeUnit.SECONDS);
        HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        httpClient.addInterceptor(interceptor);
        httpClient.addInterceptor((new Interceptor() {
            @NotNull
            public Response intercept(@NotNull Chain chain) throws IOException {
                Request original = chain.request();
                Request.Builder requestBuilder = original.newBuilder()
                        .addHeader("Content-Type", "application/json");
                Request request = requestBuilder.build();
                return chain.proceed(request);
            }
        }));
        this.okHttpClient = httpClient.build();
    }
}
