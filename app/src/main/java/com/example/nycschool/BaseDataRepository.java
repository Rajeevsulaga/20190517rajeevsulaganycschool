package com.example.nycschool;

import android.app.Application;
import android.content.Context;
import android.content.SharedPreferences;
import com.example.nycschool.utils.Constants;

abstract public class BaseDataRepository {

    private SharedPreferences sharedPreferences;

    public BaseDataRepository(Application application) {
        sharedPreferences = application.getSharedPreferences(Constants.SHARED_PREFS, Context.MODE_PRIVATE);
    }

    protected void setDataFetched(String key, boolean isDataFetched) {
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putBoolean(key, isDataFetched);
        editor.commit();
    }

    protected boolean isDataFetched(String key) {
        return sharedPreferences.getBoolean(key, false);
    }

}
