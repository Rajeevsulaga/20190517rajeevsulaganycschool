package com.example.nycschool.database;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;
import com.example.nycschool.schoolDetails.SchoolsDetails;
import com.example.nycschool.schoolList.SchoolData;

import java.util.List;

@Dao
public interface SchoolDao {

    @Insert
    void insertAll(SchoolData... schoolData);

    @Insert
    void insertAll(SchoolsDetails... schoolsDetails);

    @Query("SELECT * FROM school_data")
    LiveData<List<SchoolData>> getSchools();

    @Query("SELECT * FROM school_details WHERE dbn= :dbn ")
    SchoolsDetails getSchoolDetails(String dbn);


}
