package com.example.nycschool.database;

import android.content.Context;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import com.example.nycschool.schoolDetails.SchoolsDetails;
import com.example.nycschool.schoolList.SchoolData;

@Database(entities = { SchoolData.class, SchoolsDetails.class}, version = 1)
public abstract class SchoolDatabase extends RoomDatabase {
    private static final String DB_NAME = "school_db";

    private static SchoolDatabase INSTANCE = null;

    public static synchronized SchoolDatabase getDatabase(Context context) {
        if (null == INSTANCE) {
            INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                    SchoolDatabase.class,
                    DB_NAME).build();
        }
        return INSTANCE;
    }

    public abstract SchoolDao schoolDao();

}
