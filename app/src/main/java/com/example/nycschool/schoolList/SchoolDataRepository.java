package com.example.nycschool.schoolList;

import android.app.Application;
import android.os.AsyncTask;
import android.util.Log;
import androidx.annotation.WorkerThread;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import com.example.nycschool.BaseDataRepository;
import com.example.nycschool.api.APIClient;
import com.example.nycschool.api.SchoolDataService;
import com.example.nycschool.database.SchoolDao;
import com.example.nycschool.database.SchoolDatabase;
import com.example.nycschool.schoolDetails.SchoolsDetails;
import com.example.nycschool.utils.Constants;
import io.reactivex.observers.DisposableSingleObserver;
import io.reactivex.schedulers.Schedulers;

import java.util.List;

public class SchoolDataRepository extends BaseDataRepository {

    private static final String TAG = SchoolDataRepository.class.getSimpleName();
    private SchoolDao schoolDao;
    private SchoolDataService dataService;
    private MutableLiveData<SchoolsDetails> schoolsDetailsLiveData;


    public SchoolDataRepository(Application application) {
        super(application);
        dataService = new APIClient()
                .getClient(application.getApplicationContext(), null)
                .create(SchoolDataService.class);
        schoolDao = SchoolDatabase.getDatabase(application.getApplicationContext()).schoolDao();
        schoolsDetailsLiveData = new MutableLiveData<>();
    }

    /**
     * Fetches school data from network and saves in the database
     */
    @WorkerThread
    private void fetchSchoolData() {
        dataService.fetchSchoolData()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribeWith(new DisposableSingleObserver<List<SchoolData>>() {
                    @Override
                    public void onSuccess(List<SchoolData> value) {
                        //Save the data in the database
                        schoolDao.insertAll(value.toArray(new SchoolData[value.size()]));
                        setDataFetched(Constants.KEY_SCHOOL_DATA_FETCHED, true);
                        //Fetch school details data
                        if (!isDataFetched(Constants.KEY_SCHOOL_DETAIL_FETCHED)) {
                            fetchSchoolDetails();
                        }
                    }

                    @Override
                    public void onError(Throwable e) {
                        setDataFetched(Constants.KEY_SCHOOL_DATA_FETCHED, false);
                        Log.e(TAG, e.getMessage());
                    }
                });
    }

    /**
     * Fetches school details data from network and saves in the database
     */
    @WorkerThread
    private void fetchSchoolDetails() {
        dataService.fetchSchoolDetails()
                .subscribeOn(Schedulers.io())
                .observeOn(Schedulers.io())
                .subscribeWith(new DisposableSingleObserver<List<SchoolsDetails>>() {
                    @Override
                    public void onSuccess(List<SchoolsDetails> value) {
                        //Save the data in the database
                        schoolDao.insertAll(value.toArray(new SchoolsDetails[value.size()]));
                        setDataFetched(Constants.KEY_SCHOOL_DETAIL_FETCHED, true);
                    }

                    @Override
                    public void onError(Throwable e) {
                        setDataFetched(Constants.KEY_SCHOOL_DETAIL_FETCHED, false);
                        Log.e(TAG, e.getMessage());
                    }
                });
    }


    public LiveData<List<SchoolData>> getSchools() {
        if (!isDataFetched(Constants.KEY_SCHOOL_DATA_FETCHED)) {
            //Fetch School data from network
            fetchSchoolData();
        }
        return schoolDao.getSchools();
    }

    public void fetchSchoolDetails(final SchoolData schoolData){
        new GetSchoolDetailsTask(schoolData.getDbn()).execute();
    }

    public LiveData<SchoolsDetails> getSchoolDetails(SchoolData schoolData) {
        return schoolsDetailsLiveData;
    }

    /**
     * Fetches data from db in background thread and updates the livedata
     */
    private class GetSchoolDetailsTask extends AsyncTask<Void, Void, SchoolsDetails>{

        private String dbn;

        public GetSchoolDetailsTask(String dbn){
            this.dbn = dbn;
        }

        @Override
        protected SchoolsDetails doInBackground(Void... voids) {
            return schoolDao.getSchoolDetails(dbn);
        }

        @Override
        protected void onPostExecute(SchoolsDetails schoolsDetails) {
            schoolsDetailsLiveData.setValue(schoolsDetails);
        }
    }

}
