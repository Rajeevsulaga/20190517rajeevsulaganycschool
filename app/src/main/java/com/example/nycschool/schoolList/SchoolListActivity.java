package com.example.nycschool.schoolList;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import com.example.nycschool.R;
import com.example.nycschool.schoolDetails.SchoolDetailsActivity;
import com.example.nycschool.schoolDetails.SchoolsDetails;
import com.example.nycschool.utils.Constants;

import java.util.ArrayList;
import java.util.List;

public class SchoolListActivity extends AppCompatActivity implements SchoolsAdapter.ISchoolItemClicked {

    private RecyclerView recyclerView;
    private ProgressBar progressBar;
    private SchoolsAdapter mAdapter;

    private SchoolListViewModel schoolListViewModel;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_school_list);

        schoolListViewModel = ViewModelProviders.of(this).get(SchoolListViewModel.class);


        recyclerView = findViewById(R.id.rv_school_list);
        progressBar = findViewById(R.id.progress_bar);
        progressBar.setVisibility(View.VISIBLE);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(layoutManager);

        //Observe for the data and update UI accordingly
        progressBar.setVisibility(View.VISIBLE);
        schoolListViewModel.getSchoolList().observe(this, new Observer<List<SchoolData>>() {
            @Override
            public void onChanged(List<SchoolData> schoolData) {
                progressBar.setVisibility(View.GONE);
                mAdapter.updateList(schoolData);
            }
        });

        mAdapter = new SchoolsAdapter(new ArrayList<SchoolData>(), this, this);
        recyclerView.setAdapter(mAdapter);
        Toast.makeText(getApplicationContext(),"list added",Toast.LENGTH_SHORT).show();
    }

    public void showSchoolDetails(final SchoolData schoolData) {
        schoolListViewModel.getSchoolDetails(schoolData).observe(this, new Observer<SchoolsDetails>() {
            @Override
            public void onChanged(SchoolsDetails schoolsDetails) {
                Intent intent = new Intent(SchoolListActivity.this, SchoolDetailsActivity.class);
                Bundle bundle = new Bundle();
                bundle.putParcelable(Constants.KEY_SCHOOL_DATA, schoolData);
                bundle.putParcelable(Constants.KEY_SCHOOL_DETAIL, schoolsDetails);
                intent.putExtras(bundle);
                getApplication().startActivity(intent);
            }
        });

    }

}
