package com.example.nycschool.schoolList;

import android.app.Application;
import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.lifecycle.ViewModel;
import com.example.nycschool.schoolDetails.SchoolsDetails;

import java.util.List;

public class SchoolListViewModel extends AndroidViewModel {

    SchoolDataRepository schoolDataRepository;

    public SchoolListViewModel(@NonNull Application application) {
        super(application);
        schoolDataRepository = new SchoolDataRepository(application);
    }

    public LiveData<List<SchoolData>> getSchoolList(){
        return schoolDataRepository.getSchools();
    }

    public LiveData<SchoolsDetails> getSchoolDetails(SchoolData schoolData){
        fetchSchoolDetails(schoolData);
        return schoolDataRepository.getSchoolDetails(schoolData);
    }

    private void fetchSchoolDetails(SchoolData schoolData){
        schoolDataRepository.fetchSchoolDetails(schoolData);
    }

}
