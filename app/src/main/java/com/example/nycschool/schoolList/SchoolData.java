package com.example.nycschool.schoolList;


import android.os.Parcel;
import android.os.Parcelable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import com.example.nycschool.utils.Constants;
import com.google.gson.annotations.SerializedName;
import org.jetbrains.annotations.NotNull;

/**
 * The type School data model.
 */
@Entity(tableName = "school_data")
public class SchoolData implements Parcelable {

    @SerializedName(Constants.SCHOOL_NAME)
    private String mName;

    @PrimaryKey
    @NotNull
    @SerializedName(Constants.SCHOOL_DBN)
    private String mDbn;

    @SerializedName(Constants.SCHOOL_OVERVIEW)
    private String mOverview;

    @SerializedName(Constants.SCHOOL_NUMBER)
    private String mNumber;

    @SerializedName(Constants.SCHOOL_ADDRESS)
    private String mAddress;

    @SerializedName(Constants.SCHOOL_CITY)
    private String mCity;

    public SchoolData(){

    }

    protected SchoolData(Parcel in) {
        mName = in.readString();
        mDbn = in.readString();
        mOverview = in.readString();
        mNumber = in.readString();
        mAddress = in.readString();
        mCity = in.readString();
    }

    public static final Creator<SchoolData> CREATOR = new Creator<SchoolData>() {
        @Override
        public SchoolData createFromParcel(Parcel in) {
            return new SchoolData(in);
        }

        @Override
        public SchoolData[] newArray(int size) {
            return new SchoolData[size];
        }
    };

    public String getName() {
        return mName;
    }

    public void setName(String name) {
        this.mName = name;
    }

    public String getDbn() {
        return mDbn;
    }

    public void setDbn(String dbn) {
        this.mDbn = dbn;
    }

    public String getOverview() {
        return mOverview;
    }

    public void setOverview(String mOverview) {
        this.mOverview = mOverview;
    }

    public String getNumber() {
        return mNumber;
    }

    public void setNumber(String mNumber) {
        this.mNumber = mNumber;
    }

    public String getAddress() {
        return mAddress;
    }

    public void setAddress(String mAddress) {
        this.mAddress = mAddress;
    }

    public String getCity() {
        return mCity;
    }

    public void setCity(String mCity) {
        this.mCity = mCity;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(mName);
        dest.writeString(mDbn);
        dest.writeString(mOverview);
        dest.writeString(mNumber);
        dest.writeString(mAddress);
        dest.writeString(mCity);
    }
}
