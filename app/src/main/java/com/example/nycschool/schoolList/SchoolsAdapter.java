package com.example.nycschool.schoolList;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintLayout;
import androidx.recyclerview.widget.RecyclerView;
import com.example.nycschool.R;

import java.util.List;


/**
 * Adapter for Recycler view
 */
public class SchoolsAdapter extends RecyclerView.Adapter<SchoolsAdapter.SchoolsViewHolder> {

    private List<SchoolData> schoolsList;
    private Context context;
    private ISchoolItemClicked schoolItemClicked;

    /**
     * Instantiates a new Schools adapter.
     *
     * @param schoolsList list of all the schools
     * @param context     the context
     */
    public SchoolsAdapter(List<SchoolData> schoolsList, Context context, ISchoolItemClicked itemClicked) {
        this.schoolsList = schoolsList;
        this.context = context;
        this.schoolItemClicked = itemClicked;
    }

    @Override
    public SchoolsViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.school_list_item, parent, false);
        return new SchoolsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(SchoolsViewHolder holder, final int position) {
        holder.bind(schoolsList.get(position));
        holder.parentLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               schoolItemClicked.showSchoolDetails(schoolsList.get(position));
            }
        });
    }

    @Override
    public int getItemCount() {
        return schoolsList.size();
    }

    public void updateList(List<SchoolData> schoolData){
        this.schoolsList = schoolData;
        notifyDataSetChanged();
    }

    /**
     * Recycler view holder consisting references to all view items
     */
    public static class SchoolsViewHolder extends RecyclerView.ViewHolder {
        CardView parentLayout;
        TextView schoolName;
        TextView dbn;
        TextView address;
        TextView city;
        TextView number;
        TextView description;


        /**
         * Instantiates a new Schools view holder.
         *
         * @param v parent view for referencing all child items
         */
        public SchoolsViewHolder(View v) {
            super(v);
            parentLayout = v.findViewById(R.id.parent_view);
            schoolName = v.findViewById(R.id.tv_school_name);
            dbn = v.findViewById(R.id.tv_dbn);
            address = v.findViewById(R.id.tv_address);
            city = v.findViewById(R.id.tv_city);
            number = v.findViewById(R.id.tv_number);
            description = v.findViewById(R.id.tv_description);
        }

        public void bind(SchoolData schoolData){
            schoolName.setText(schoolData.getName());
            dbn.setText(schoolData.getDbn());
            address.setText(schoolData.getAddress());
            city.setText(schoolData.getCity());
            number.setText(schoolData.getNumber());
            description.setText(schoolData.getOverview());
        }
    }

    interface ISchoolItemClicked{
        void showSchoolDetails(SchoolData schoolData);
    }
}