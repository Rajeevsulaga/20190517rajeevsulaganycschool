package com.example.nycschool.utils;

import com.google.gson.annotations.SerializedName;

/*
* Constants Class - contains all required constants
* */
public class Constants {
    public static final String SCHOOL_NAMES_URL = "https://data.cityofnewyork.us/resource/97mf-9njv.json";
    public static final String SCHOOL_DETAILS_URL = "https://data.cityofnewyork.us/resource/f9bf-2cp4.json";

    public static final String SHARED_PREFS = "school_prefs";
    public static final String KEY_SCHOOL_DATA_FETCHED = "school_list";
    public static final String KEY_SCHOOL_DETAIL_FETCHED = "school_detail";

    public static final String KEY_SCHOOL_DATA = "school_data";
    public static final String KEY_SCHOOL_DETAIL = KEY_SCHOOL_DETAIL_FETCHED;

    public static final String SCHOOL_DBN = "dbn";
    public static final String SCHOOL_NAME = "school_name";
    public static final String SCHOOL_OVERVIEW = "overview_paragraph";
    public static final String SCHOOL_NUMBER = "phone_number";
    public static final String SCHOOL_ADDRESS = "primary_address_line_1";
    public static final String SCHOOL_CITY = "city";
    public static final String SAT_CRITICAL_RED_AVG_SCORE = "sat_critical_reading_avg_score";
    public static final String SAT_MATH_AVG_SCORE = "sat_math_avg_score";
    public static final String SAT_WRT_AVG_SCORE = "sat_writing_avg_score";


}
